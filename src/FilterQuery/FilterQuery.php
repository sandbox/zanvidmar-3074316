<?php


namespace Drupal\filter_query_api\FilterQuery;

use Drupal\filter_query_api\FilterQueryAction\FilterQueryCondition\FilterQueryCondition;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryCondition\FilterQueryConditionCondition\FilterQueryConditionCondition;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryCondition\FilterQueryConditionSorting\FilterQueryConditionSorting;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryFilter\FilterQueryFilter;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryFilter\FilterQueryFilterCondition\FilterQueryFilterCondition;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryFilter\FilterQueryFilterSorting\FilterQueryFilterSorting;
use Drupal\filter_query_api\FilterQueryPager\FilterQueryPager;

class FilterQuery {

  const DEFAULT_RESULT_RETURN_VALUE = [];

  const AJAX_CALLBACK = '::filterQueryAjaxCallback';
  const CUSTOM_AJAX_CALLBACK_KEY = 'custom_filter_query_ajax_callback';
  const DEFAULT_RESET_BUTTON_VALUE = TRUE;

  protected $entity;
  private $conditions = [];
  private $filter_conditions = [];
  private $sorting = [];
  private $filter_sorting = [];
  private $all_filters = [];
  private $all_conditions = [];
  private $pager = NULL;
  private $url_parameters = [];
  private $query_error = FALSE;
  private $reset_button = self::DEFAULT_RESET_BUTTON_VALUE;

  /**
   * FilterQuery constructor.
   * @param $entity
   */
  public function __construct() {
    $this->entity = NULL;
    $this->conditions = [];
    $this->filter_conditions = [];
    $this->sorting = [];
    $this->filter_sorting = [];
    $this->all_filters = [];
    $this->all_conditions = [];
    $this->pager = NULL;
    $this->query_error = FALSE;
    $this->reset_button = self::DEFAULT_RESET_BUTTON_VALUE;
  }

  /**
   * @param $entity
   * @return FilterQuery
   */
  public static function factory() {
    return new FilterQuery();
  }

  /**
   * Return filter parameter key from request.
   *
   * Return ajax value for ajax request.
   * Return url value for all other requests (this is fallback).
   *
   * @param $filter_parameter_key
   * @return mixed
   */
  public static function getFilterParameterKeyFromRequest($filter_parameter_key) {

    // @todo: sanitize filter?

    $request = \Drupal::request();
    $is_ajax = $request->isXmlHttpRequest();

    if ($is_ajax) {
     return $ajax_filter_value = $request->request->get($filter_parameter_key);
    } else {
     return $url_filter_value = \Drupal::request()->query->get($filter_parameter_key);
    }
  }

  /**
   * @param $entity
   */
  public function setEntity($entity) {
    $this->entity = $entity;
  }

  /**
   * @param FilterQueryConditionCondition $condition
   */
  public function setCondition(FilterQueryConditionCondition $condition) {
    $this->conditions[] = $condition;
  }

  /**
   * @param FilterQueryFilterCondition $filter
   */
  public function setFilterCondition(FilterQueryFilterCondition $filter) {
    $this->filter_conditions[$filter->url_parameter_key()] = $filter;
  }

  /**
   * @param FilterQueryConditionSorting $sorting
   */
  public function setSorting(FilterQueryConditionSorting $sorting) {
    $this->sorting[] = $sorting;
  }

  /**
   * @param FilterQueryFilterSorting $sorting
   */
  public function setFilterSorting(FilterQueryFilterSorting $sorting) {
    $this->filter_sorting[] = $sorting;
  }

  /**
   * @param FilterQueryPager $pager
   */
  public function setPager(FilterQueryPager $pager) {
    $this->pager = $pager;
  }

  /**
   * @return array
   */
  public function getUrlParameters() {
    return $this->url_parameters;
  }

  /**
   * @param $parameter_key
   * @return mixed
   */
  public function getUrlParameter($parameter_key) {
    $value = NULL;

    if (isset($this->getUrlParameters()[$parameter_key])) {
      $value = $this->getUrlParameters()[$parameter_key];
    }

    return $value;
  }

  /**
   * @return array
   */
  public function getAllFilters() {
    return $this->all_filters;
  }

  /**
   * @return array
   */
  public function getAllConditions() {
    return $this->all_conditions;
  }

  /**
   * @return array|int
   * @throws \Exception
   */
  public function results() {
    $this->_mergeFilters();
    $this->_mergeConditions();
    $this->_getUrlParameters();
    // merge again because now filters are after _getUrlParameters().
    $this->_mergeFilters();

    // Do not even run query if error is already set
    if (!$this->getQueryError()) {
      $q = \Drupal::entityQuery($this->entity);

      // CONDITIONS
      if (!empty($this->all_conditions)) {
        /** @var FilterQueryCondition $condition */
        foreach ($this->all_conditions as $condition) {
          $action_type = $condition->getQueryActionType();
          $q->$action_type($condition->query_field(),
            $condition->query_default_value(), $condition->operator(), $condition->langcode());
        }
      }

      // FILTERS
      // At this point filters are already validated

      /**
       * For the regular request, use parameters form the url as filter value
       * For the ajax request, use parameters form ajax request as filter value
       * In both cases, default filter value is fallback.
       */
      $request = \Drupal::request();
      $is_ajax = $request->isXmlHttpRequest();

      /** @var FilterQueryFilterCondition $filter */
      foreach ($this->all_filters as $filter) {
        $action_type = $filter->getQueryActionType();
        $url_parameter_key = $filter->url_parameter_key();

        if ($is_ajax) {

          // Use ajax parameter value or set the default one as fallback
          $ajax_filter_value = $request->request->get($url_parameter_key);
          if (isset($ajax_filter_value) && $ajax_filter_value !== '') {
            $filter_value = $ajax_filter_value;
          } else {
            $filter_value = $filter->query_default_value();
          }

        } else {

          // Use url parameter value or set the default one as fallback
          if (isset($this->url_parameters[$url_parameter_key])) {
            $filter_value = $this->url_parameters[$url_parameter_key];
          } else {
            $filter_value = $filter->query_default_value();
          }
        }

        if ($filter_value !== NULL) {
          $q->$action_type($filter->query_field(), $filter_value, $filter->operator(), $filter->langcode());
        }



        // @todo: remove
//        $url_parameter_key = $filter->url_parameter_key();

//        if (isset($this->url_parameters[$url_parameter_key])) {
//          $filter_value_raw = $this->url_parameters[$url_parameter_key];
//          $filter->validateUrlValue($filter_value_raw);
//
//          // Stop whole process when at least one filter is not validated.
//          if (!$filter->validated()) {
//            $this->setQueryError();
//            return self::DEFAULT_RESULT_RETURN_VALUE;
//          }
//        } else {
//          $filter_value = $filter->query_default_value();
//        }
      }

      try {
        $results = $q->execute();

      } catch (\Exception $e) {
        $this->setQueryError();
      }
    }

    // Return results array if there is no exception thrown,
    // otherwise return an empty array.
    if (isset($results)) {
      return $results;
    } else {
      return self::DEFAULT_RESULT_RETURN_VALUE;
    }
  }

  /**
   * Return error status.
   *
   * @return null/bool
   */
  public function getQueryError() {
    return $this->query_error;
  }

  /**
   * Set error status.
   */
  public function setQueryError() {
    $this->query_error = TRUE;
  }

  /**
   * Set whether reset button is visible on form or not.
   *
   * @param bool $bool
   */
  public function setResetButton(bool $bool = self::DEFAULT_RESET_BUTTON_VALUE) {
    $this->reset_button = $bool;
  }

  /**
   * Get whether reset button is visible on form or not.
   *
   * @return bool
   */
  public function getResetButton() {
    return $this->reset_button;
  }

  /**
   * @param bool $reset_button
   * @return array
   */
  public function renderFilters(bool $reset_button = self::DEFAULT_RESET_BUTTON_VALUE) {
    $this->setResetButton($reset_button);
    return \Drupal::formBuilder()->getForm('Drupal\filter_query_api\FilterQueryForm\FilterQueryForm', $this);
  }

  /**
   * Merge filters
   *  There is no difference between filters when url values are checked
   *  (or any similar process).
   */
  private function _mergeFilters() {
    $this->all_filters =
      array_merge($this->filter_conditions, $this->filter_sorting);
  }

  /**
   * Merge conditions.
   */
  private function _mergeConditions() {
    $this->all_conditions =
      array_merge($this->conditions, $this->sorting);
  }

  /**
   * Get url parameters and sanitize them.
   *
   */
  private function _getUrlParameters() {
    // Get all url parameters
    $params = \Drupal::request()->query->all();

    if (!empty($this->all_filters)) {

      /** @var FilterQueryFilter $filter */
      foreach ($this->all_filters as $filter) {

        // Get the url_parameter_key from filter object
        $filter_url_parameter_key = $filter->url_parameter_key();

        // Skip validation if there is no url parameter for this filter.
        if (isset($params[$filter_url_parameter_key])) {
          // Check if given url_parameter_key value is set in request url parameters
          // This filter prevent executing url_parameter_keys that are not defined
          // in filters
          $raw_url_filter_value = $params[$filter_url_parameter_key];

          // Validate url value
          // After the filter key is check, url values (of given key) are check.
          // This prevent execution query under filter condition that is not
          // determined.
          $filter->validateUrlValue($raw_url_filter_value);

          if ($filter->validated()) {
            $this->url_parameters[$filter_url_parameter_key]
              = $raw_url_filter_value;
          } else {
            $this->setQueryError();
          }
        }
      }
    }
  }
}
