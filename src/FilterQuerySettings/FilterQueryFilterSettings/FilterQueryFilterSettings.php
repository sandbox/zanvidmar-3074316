<?php

namespace Drupal\filter_query_api\FilterQuerySettings\FilterQueryFilterSettings;

use Drupal\filter_query_api\FilterQueryAction\FilterQueryAction;
use Drupal\filter_query_api\FilterQuerySettings\FilterQueryFilterRenderSettings\FilterQueryFilterRenderSettings;

class FilterQueryFilterSettings {

  protected $url_parameter_key;
  protected $options;
  protected $form_filter_settings;

  /**
   * FilterQueryFilterSettings constructor.
   * @param $url_parameter_key
   * @param $options
   * @param FilterQueryFilterRenderSettings $form_filter_settings
   */
  public function __construct($url_parameter_key, $options, FilterQueryFilterRenderSettings $form_filter_settings = FilterQueryAction::DEFAULT__FILTER_SETTINGS) {
    $this->url_parameter_key = $url_parameter_key;
    $this->options = $options;
    $this->form_filter_settings = $form_filter_settings;
  }

  /**
   * @param $url_parameter_key
   * @param $options
   * @param FilterQueryFilterRenderSettings $form_filter_settings
   * @return FilterQueryFilterSettings
   */
  public static function factory($url_parameter_key, $options, FilterQueryFilterRenderSettings $form_filter_settings = FilterQueryAction::DEFAULT__FILTER_SETTINGS) {
    return new FilterQueryFilterSettings($url_parameter_key, $options, $form_filter_settings);
  }

  /**
   * @return mixed
   */
  public function url_parameter_key() {
    return $this->url_parameter_key;
  }

  /**
   * @return mixed
   */
  public function options() {
    return $this->options;
  }

  /**
   * @return mixed
   */
  public function form_filter_settings() {
    return $this->form_filter_settings;
  }

}