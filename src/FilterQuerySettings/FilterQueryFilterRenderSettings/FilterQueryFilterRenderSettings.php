<?php

namespace Drupal\filter_query_api\FilterQuerySettings\FilterQueryFilterRenderSettings;

class FilterQueryFilterRenderSettings {

  protected $title;
  protected $description;
  protected $render_element_type;
  protected $render_element_custom_data;

  /**
   * FilterQueryFilterRenderSettings constructor.
   * @param $title
   * @param $description
   * @param string $render_element_type
   * @param null $render_element_custom_data
   */
  public function __construct($title, $description, $render_element_type = 'select', $render_element_custom_data = NULL) {
    $this->title = $title;
    $this->description = $description;
    $this->render_element_type = $render_element_type;
    $this->render_element_custom_data = $render_element_custom_data;
  }

  /**
   * @param $title
   * @param $description
   * @param string $render_element_type
   * @param array $render_element_custom_data
   * @return FilterQueryFilterRenderSettings
   */
  public static function factory($title, $description, $render_element_type = 'select', array $render_element_custom_data = []) {
    return new FilterQueryFilterRenderSettings($title, $description, $render_element_type, $render_element_custom_data);
  }


  /**
   * @return mixed
   */
  public function title() {
    return $this->title;
  }

  /**
   * @return mixed
   */
  public function description() {
    return $this->description;
  }

  /**
   * @return mixed
   */
  public function render_element_type() {
    return $this->render_element_type;
  }

  /**
   * @return array
   */
  public function render_element_custom_data() {
    return $this->render_element_custom_data;
  }

}
