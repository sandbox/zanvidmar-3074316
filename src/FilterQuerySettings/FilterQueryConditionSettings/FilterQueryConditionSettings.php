<?php

namespace Drupal\filter_query_api\FilterQuerySettings\FilterQueryConditionSettings;

use Drupal\filter_query_api\FilterQueryAction\FilterQueryAction;

class FilterQueryConditionSettings {

  protected $query_field;
  protected $query_default_value;
  protected $operator;
  protected $langcode;

  /**
   * FilterQueryCondition constructor.
   * @param $query_field
   * @param $query_default_value
   * @param null $operator
   * @param null $langcode
   */
  public function __construct($query_field, $query_default_value, $operator = FilterQueryAction::DEFAULT__OPERATOR, $langcode = FilterQueryAction::DEFAULT__LANGCODE) {
    $this->query_field = $query_field;
    $this->query_default_value = $query_default_value;
    $this->operator = $operator;
    $this->langcode = $langcode;
  }

  /**
   * @param $query_field
   * @param $query_default_value
   * @param null $operator
   * @param null $langcode
   * @return FilterQueryConditionSettings
   */
  public static function factory($query_field, $query_default_value, $operator = FilterQueryAction::DEFAULT__OPERATOR, $langcode = FilterQueryAction::DEFAULT__LANGCODE) {
    return new FilterQueryConditionSettings($query_field, $query_default_value, $operator, $langcode);
  }

  /**
   * @return mixed
   */
  public function query_field() {
    return $this->query_field;
  }

  /**
   * @return mixed
   */
  public function query_default_value() {
    return $this->query_default_value;
  }

  /**
   * @return null
   */
  public function operator() {
    return $this->operator;
  }

  /**
   * @return null
   */
  public function langcode() {
    return $this->langcode;
  }

}
