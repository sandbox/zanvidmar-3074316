<?php


namespace Drupal\filter_query_api\FilterQueryAction;

use Drupal\filter_query_api\FilterQueryAction\FilterQueryCondition\FilterQueryCondition;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryCondition\FilterQueryConditionCondition\FilterQueryConditionCondition;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryCondition\FilterQueryConditionSorting\FilterQueryConditionSorting;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryFilter\FilterQueryFilterCondition\FilterQueryFilterCondition;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryFilter\FilterQueryFilterSorting\FilterQueryFilterSorting;
use Drupal\filter_query_api\FilterQuerySettings\FilterQueryConditionSettings\FilterQueryConditionSettings;
use Drupal\filter_query_api\FilterQuerySettings\FilterQueryFilterSettings\FilterQueryFilterSettings;
use Drupal\filter_query_api\FilterQuerySettings\FilterQueryFilterRenderSettings\FilterQueryFilterRenderSettings;

class FilterQueryAction {

  const DEFAULT__QUERY_FIELD = '';
  const DEFAULT__DEFAULT_VALUE = '';
  const DEFAULT__URL_PARAMETER_KEY = '';
  const DEFAULT__OPTIONS = [];
  const DEFAULT__OPERATOR = NULL;
  const DEFAULT__FILTER_SETTINGS = NULL;
  const DEFAULT__LANGCODE = NULL;
  const DEFAULT__VALIDATED  = TRUE;

  protected $query_field;
  protected $query_default_value;
  protected $form_filter_settings;
  protected $url_parameter_key;
  protected $options = [];
  protected $operator = NULL;
  protected $langcode = NULL;
  protected $validated = TRUE;

  /**
   * FilterQueryAction constructor.
   * @param FilterQueryConditionSettings $condition
   * @param FilterQueryFilterSettings $filter_settings
   */
  public function __construct(FilterQueryConditionSettings $condition, FilterQueryFilterSettings $filter_settings){
    $condition ? $this->query_field = $condition->query_field() : NULL;
    $condition ? $this->query_default_value = $condition->query_default_value() : NULL;
    $filter_settings ? $this->url_parameter_key = $filter_settings->url_parameter_key() : NULL;
    $filter_settings ? $this->options = $filter_settings->options() : NULL;
    $condition ? $this->operator = $condition->operator() : NULL;
    $filter_settings ? $this->form_filter_settings = $filter_settings->form_filter_settings() : NULL;
    $condition ? $this->langcode = $condition->langcode() : NULL;
    $this->validated = TRUE;
  }

  /**
   * @return mixed
   */
  public function query_field() {
    return $this->query_field;
  }

  /**
   * @return mixed
   */
  public function query_default_value() {
    return $this->query_default_value;
  }

  /**
   * @return mixed
   */
  public function url_parameter_key() {
    return $this->url_parameter_key;
  }

  /**
   * @return mixed
   */
  public function options() {
    return $this->options;
  }

  /**
   * @return null
   */
  public function operator() {
    return $this->operator;
  }

  /**
   * @return FilterQueryFilterRenderSettings
   */
  public function form_filter_settings() {
    return $this->form_filter_settings;
  }

  /**
   * @return null
   */
  public function langcode() {
    return $this->langcode;
  }

  /**
   * @return bool
   */
  public function validated() {
    return $this->validated;
  }

  /**
   * Get action type 'sort' or 'condition' based on object type.
   *  This is helpful to determine what type on action should be triggered
   *  on main entity query.
   *
   * @return string
   * @throws \Exception
   */
  public function getQueryActionType() {

    if (
      $this instanceof FilterQueryConditionCondition ||
      $this instanceof FilterQueryFilterCondition
    ) {
      $action_type = 'condition';
    } elseif (
      $this instanceof FilterQueryConditionSorting ||
      $this instanceof FilterQueryFilterSorting
    ) {
      $action_type = 'sort';
    } else {
      throw new \Exception('$action_type value is not determined.');
    }

    return $action_type;
  }

}
