<?php

namespace Drupal\filter_query_api\FilterQueryAction\FilterQueryFilter;

use Drupal\filter_query_api\FilterQueryAction\FilterQueryAction;
use Drupal\filter_query_api\FilterQuerySettings\FilterQueryConditionSettings\FilterQueryConditionSettings;
use Drupal\filter_query_api\FilterQuerySettings\FilterQueryFilterSettings\FilterQueryFilterSettings;

class FilterQueryFilter extends FilterQueryAction {

  /**
   * @inheritDoc
   */
  public function __construct(FilterQueryConditionSettings $condition, FilterQueryFilterSettings $filter_settings) {
    parent::__construct($condition, $filter_settings);
  }

  /**
   * @inheritDoc
   */
  public static function factory(FilterQueryConditionSettings $condition, FilterQueryFilterSettings $filter_settings) {
    return new FilterQueryFilter($condition, $filter_settings);
  }

  /**
   * Validate value from url.
   *
   * @param $value_to_validate
   */
  public function validateUrlValue($value_to_validate) {
    if ($this->options === FALSE) {
      // this filter does not require validation
      $this->validated = TRUE;
    } elseif (in_array($value_to_validate, array_keys($this->options))) {
      $this->validated = TRUE;
    } else {
      $this->validated = FALSE;
    }
  }

}
