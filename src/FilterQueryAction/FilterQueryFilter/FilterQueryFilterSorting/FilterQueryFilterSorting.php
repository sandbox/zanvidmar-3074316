<?php

namespace Drupal\filter_query_api\FilterQueryAction\FilterQueryFilter\FilterQueryFilterSorting;

use Drupal\filter_query_api\FilterQueryAction\FilterQueryFilter\FilterQueryFilter;
use Drupal\filter_query_api\FilterQuerySettings\FilterQueryConditionSettings\FilterQueryConditionSettings;
use Drupal\filter_query_api\FilterQuerySettings\FilterQueryFilterSettings\FilterQueryFilterSettings;

class FilterQueryFilterSorting extends FilterQueryFilter {

  /**
   * @inheritDoc
   */
  public function __construct(FilterQueryConditionSettings $condition, FilterQueryFilterSettings $filter_settings) {
    parent::__construct($condition, $filter_settings);
  }

  /**
   * @inheritDoc
   */
  public static function factory(FilterQueryConditionSettings $condition, FilterQueryFilterSettings $filter_settings) {
    return new FilterQueryFilterSorting($condition, $filter_settings);
  }

}
