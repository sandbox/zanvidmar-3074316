<?php

namespace Drupal\filter_query_api\FilterQueryAction\FilterQueryCondition\FilterQueryConditionCondition;

use Drupal\filter_query_api\FilterQueryAction\FilterQueryAction;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryCondition\FilterQueryCondition;

class FilterQueryConditionCondition extends FilterQueryCondition {

  /**
   * @inheritDoc
   */
  public function __construct($query_field, $query_default_value, $operator = FilterQueryAction::DEFAULT__OPERATOR, $langcode = FilterQueryAction::DEFAULT__LANGCODE) {
    parent::__construct($query_field, $query_default_value, $operator, $langcode);
  }

  /**
   * @inheritDoc
   */
  public static function factory($query_field, $query_default_value, $operator = FilterQueryAction::DEFAULT__OPERATOR, $langcode = FilterQueryAction::DEFAULT__LANGCODE) {
    return new FilterQueryConditionCondition($query_field, $query_default_value, $operator, $langcode);
  }

}