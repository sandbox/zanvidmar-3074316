<?php

namespace Drupal\filter_query_api\FilterQueryAction\FilterQueryCondition\FilterQueryConditionSorting;

use Drupal\filter_query_api\FilterQueryAction\FilterQueryAction;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryCondition\FilterQueryCondition;

class FilterQueryConditionSorting extends FilterQueryCondition {

  /**
   * @inheritDoc
   */
  public function __construct($query_field, $query_default_value, $langcode = FilterQueryAction::DEFAULT__LANGCODE) {
    parent::__construct($query_field, $query_default_value, FilterQueryAction::DEFAULT__OPERATOR, $langcode);
  }

  /**
   * @inheritDoc
   */
  public static function factory($query_field, $query_default_value, $operator = FilterQueryAction::DEFAULT__OPERATOR, $langcode = FilterQueryAction::DEFAULT__LANGCODE) {
    return new FilterQueryConditionSorting($query_field, $query_default_value, $langcode);
  }

}
