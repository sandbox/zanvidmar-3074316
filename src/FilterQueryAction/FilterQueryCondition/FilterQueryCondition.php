<?php

namespace Drupal\filter_query_api\FilterQueryAction\FilterQueryCondition;

use Drupal\filter_query_api\FilterQueryAction\FilterQueryAction;

use Drupal\filter_query_api\FilterQuerySettings\FilterQueryConditionSettings\FilterQueryConditionSettings;
use Drupal\filter_query_api\FilterQuerySettings\FilterQueryFilterSettings\FilterQueryFilterSettings;

class FilterQueryCondition extends FilterQueryAction {

  /**
   * @inheritDoc
   */
  public function __construct($query_field, $query_default_value, $operator = FilterQueryAction::DEFAULT__OPERATOR, $langcode = FilterQueryAction::DEFAULT__LANGCODE) {
    parent::__construct(
      FilterQueryConditionSettings::factory($query_field, $query_default_value, $operator, $langcode),
      FilterQueryFilterSettings::factory(FilterQueryAction::DEFAULT__URL_PARAMETER_KEY, FilterQueryAction::DEFAULT__OPTIONS)
    );
  }

  /**
   * @inheritDoc
   */
  public static function factory($query_field, $query_default_value, $operator = FilterQueryAction::DEFAULT__OPERATOR, $langcode = FilterQueryAction::DEFAULT__LANGCODE) {
    return new FilterQueryCondition(
      FilterQueryConditionSettings::factory($query_field, $query_default_value, $operator, $langcode),
      FilterQueryFilterSettings::factory(FilterQueryAction::DEFAULT__URL_PARAMETER_KEY, FilterQueryAction::DEFAULT__OPTIONS)
    );
  }

}
