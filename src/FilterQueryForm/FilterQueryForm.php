<?php

namespace Drupal\filter_query_api\FilterQueryForm;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter_query_api\FilterQuery\FilterQuery;
use Drupal\filter_query_api\FilterQueryAction\FilterQueryFilter\FilterQueryFilter;
use Symfony\Component\HttpFoundation\ParameterBag;

class FilterQueryForm extends FormBase  {

  const EMPTY_VALUE = '';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'filter_query_form';
  }

  /**
   * {@inheritdoc}
   *
   * $entity has to be defined because it is in construct of this class
   */
  public function buildForm(array $form, FormStateInterface $form_state, FilterQuery $filterQuery = NULL) {
    $form_state->disableCache(); // to prevent cached data on ajax request.

    /** @var FilterQueryFilter $filter */
    foreach ($filterQuery->getAllFilters() as $filter) {

      $form[$filter->url_parameter_key()] = [
        '#type' => !empty($filter->form_filter_settings()->render_element_type()) ?
          $filter->form_filter_settings()->render_element_type() : 'select',
        '#title' => t($filter->form_filter_settings()->title()),
        '#description' => !empty($filter->form_filter_settings()->description()) ?
          t($filter->form_filter_settings()->description()) : '',
        '#options' => $filter->options() ? $filter->options() : NULL,
        '#default_value' => isset($filterQuery->getUrlParameters()[$filter->url_parameter_key()]) ?
          $filterQuery->getUrlParameters()[$filter->url_parameter_key()] : $filter->query_default_value(),
        '#empty_option' => t('- Select -'),
        '#empty_value' => self::EMPTY_VALUE
      ];

      // This is function allows user to add any kind of custom data to render array
      // like ajax request or even overriding #title,... ect.
      if (!empty($filter->form_filter_settings()->render_element_custom_data())) {
        $existing_data = $form[$filter->url_parameter_key()];
        $custom_data = $filter->form_filter_settings()->render_element_custom_data();
        $overridden_data = array_replace($existing_data, $custom_data);
        $form[$filter->url_parameter_key()] = $overridden_data;
      }
    }

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );


    if ($filterQuery->getResetButton() === TRUE) {
      $current_path = \Drupal::service('path.current')->getPath();
      $form['actions']['reset'] = [
        '#type' => 'html_tag',
        '#tag' => 'a',
        '#value' => $this->t('Reset'),
        '#attributes' => [
          'class' => ['button', 'reset-button'],
          'href' => $current_path
        ],
      ];
    }

    // Pass FilterQuery object to submit form
    $form_state->set('filterQuery', $filterQuery);

    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Pick FilterQuery object from form_state
    $filterQuery = $form_state->get('filterQuery');
    $route_name = \Drupal::routeMatch()->getRouteName();
    /** @var ParameterBag $route_parameters */
    $route_parameters = \Drupal::routeMatch()->getParameters();

    if (!empty($route_parameters->all())) {
      $parameters = $route_parameters->all();
    } else {
      $parameters = [];
    }

    // @todo: I think we will get node parameter form route_parameters. (need to be tested)
//    $node = \Drupal::routeMatch()->getParameter('node');
//    if ($node instanceof NodeInterface) {
//      $nid = $node->id();
//    }
//    // Nid is default
//    $parameters['node'] = $nid;

    /** @var FilterQueryFilter $filter */
    foreach ($filterQuery->getAllFilters() as $filter) {
      $form_key = $filter->url_parameter_key();

      if ($form_state->getValue($form_key) !== NULL && !empty($form_state->getValue($form_key))) {
        $parameters[$form_key] = $form_state->getValue($form_key);
      }
    }

    // Set redirect with non-empty parameters.
    $form_state->setRedirect($route_name, $parameters);
  }

  /**
   * Function that encapsulate custom callback function from ajax callback on
   * filter form element.
   *
   * Ajax example with custom function and data:
   *  '#ajax'=> [
   *   'callback' => FilterQuery::AJAX_CALLBACK,
   *    FilterQuery::CUSTOM_AJAX_CALLBACK_KEY => ['Drupal\example\Example::ajaxExampleCallback', ['custom', 'data']],
   *    ...
   *  ]
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return mixed
   * @throws \Exception
   */
  public function filterQueryAjaxCallback(array $form, FormStateInterface $form_state) {
    $trigger_element = $form_state->getTriggeringElement();

    if (!empty($trigger_element) && isset($trigger_element['#ajax'][FilterQuery::CUSTOM_AJAX_CALLBACK_KEY])) {
      $custom_callback = $trigger_element['#ajax'][FilterQuery::CUSTOM_AJAX_CALLBACK_KEY];
      list($callback_function, $callback_data) = $custom_callback;

      if (is_callable($callback_function)) {
        $data = [
          'form' => $form,
          'form_state' => $form_state,
          FilterQuery::CUSTOM_AJAX_CALLBACK_KEY . '_data' => $callback_data
        ];
        return call_user_func($callback_function, $data);
      } else {
        throw new \Exception('filterQueryAjaxCallback: callback function is not callable.');
      }
    } else {
      throw new \Exception('filterQueryAjaxCallback: filterQueryAjaxCallback requires "' . FilterQuery::CUSTOM_AJAX_CALLBACK_KEY . '"" parameter');
    }
  }

}