<?php

namespace Drupal\filter_query_api\FilterQueryPager;

class FilterQueryPager {

  protected $entity;

  /**
   * FilterQueryPager constructor.
   * @param $entity
   */
  public function __construct($entity) {

  }

  /**
   * @return FilterQueryPager
   */
  public static function factory($entity) {
    return new FilterQueryPager($entity);
  }

}